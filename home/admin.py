from django.contrib import admin
from home.models import Country, Region,Tuman, Nationality

# Register your models here.

# admin.site.register(Branch)
# admin.site.register(Application)
admin.site.register(Country)
admin.site.register(Region)
admin.site.register(Tuman)
admin.site.register(Nationality)