# Generated by Django 3.2.4 on 2021-07-20 16:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0003_application_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='application',
            name='status',
            field=models.CharField(blank=True, choices=[('New', 'Yangi'), ('Under consideration', "Ko'rib chiqilmoqda"), ('Accepted', 'Qabul qilindi'), ('Rejection', 'Qaytarildi')], default='New', max_length=50, null=True),
        ),
    ]
