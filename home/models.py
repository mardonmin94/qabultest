from django.db import models
from django.forms import ModelForm, Select, TextInput, FileInput
from django import forms

# Create your models here.

TYPE_GENDER = [
        ('Мужчина', 'Эркак'),
        ('Женщина', 'Аёл'),
    ]

STATUS = [
        ('New', 'Yangi'),
        ('Under consideration', 'Ko\'rib chiqilmoqda'),
        ('Accepted', 'Qabul qilindi'),
        ('Rejection', 'Qaytarildi'),
    ]



class Country(models.Model):
    name = models.CharField(max_length=200, null=True, unique=True)

    def __str__(self):
        return f"{self.name}"

class Region(models.Model):
    # country = models.ForeignKey(Country, on_delete=models.CASCADE, null=True,)
    name = models.CharField(max_length=200, null=True, unique=True)

    def __str__(self):
        return f"{self.name}"

class Tuman(models.Model):
    region = models.ForeignKey(Region, on_delete=models.SET_NULL, null=True)
    name = models.CharField(max_length=200, null=True, unique=True)

    def __str__(self):
        return f"{self.name}"

class Nationality(models.Model):
    nationality = models.CharField(max_length=100, null=True)

    def __str__(self):
        return f"{self.nationality}"
