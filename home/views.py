from django.views.generic import CreateView
from django.urls import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from home.models import Country, Region, Tuman
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import messages
import json
from bachelor.models import Application
from magistr.models import Magapplication


# Create your views here.

def index_view(request):
    return render(request, 'index.html')

#####

# def list_app(request):
#     bapps = Application.objects.all()
#     mapps = Magapplication.objects.all()
#
#     return render(request, 'list_b.html', {'bapps':bapps, 'mapps': mapps})



# def upload(request):
#     if request.method == 'POST':
#         uploaded_file = request.FILES['document']
#         print(uploaded_file.name)
#         print(uploaded_file.size)
#     return render(request, 'upload.html')

