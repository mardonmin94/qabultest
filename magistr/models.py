from django.db import models
from home.models import STATUS, TYPE_GENDER, Country, Region, Tuman, Nationality


# Create your models here.

class Branch(models.Model):
    barnch_name = models.CharField(max_length=299, null=True)

    def __str__(self):
        return f'{self.barnch_name}'


class Magapplication(models.Model):
    branch_name = models.ForeignKey(Branch, on_delete=models.CASCADE, null=True, verbose_name='Выберите специальность')
    last_name = models.CharField(max_length=50, null=True, verbose_name='Фамилия')
    first_name = models.CharField(max_length=50, null=True, verbose_name='Имя')
    sure_name = models.CharField(max_length=50, null=True, verbose_name='Отчество')
    date_of_birth = models.DateField(null=True, verbose_name='Дата рождения')
    gender = models.CharField(max_length=20, choices=TYPE_GENDER, verbose_name='Выберите пол', null=True)
    photo = models.ImageField(upload_to='photo/', null=True, verbose_name='Фотография 3х4 (jpg, jpeg, png)')
    ####
    country = models.ForeignKey(Country, on_delete=models.CASCADE, null=True, verbose_name='Гражданство')
    nationality = models.ForeignKey(Nationality, on_delete=models.CASCADE, null=True, blank=True, verbose_name='Национальность')
    region = models.ForeignKey(Region, on_delete=models.SET_NULL, null=True, verbose_name='Область')
    tuman = models.ForeignKey(Tuman, on_delete=models.SET_NULL, null=True, verbose_name='Район')
    adress = models.CharField(max_length=499, null=True, verbose_name='Улица, дом, квартира')
    pasport = models.CharField(max_length=9, null=True, unique=True, verbose_name='Серия и номер паспорта')

    pasport_image = models.ImageField(upload_to='pasport/', null=True, verbose_name='Электронный образ паспорта')
    ####
    # type_institution = models.CharField(choices=TYPE_INSTITUTION, max_length=60, null=True, verbose_name='Тип заведения')

    name_institution = models.CharField(max_length=299, null=True, verbose_name='Название учреждения')
    year = models.CharField(max_length=4, null=True, verbose_name='Год выпуска')
    diplom = models.CharField(max_length=10, null=True, verbose_name='Серия и номер диплома или аттестата')
    diplom_photo = models.ImageField(upload_to='diplom/', null=True, verbose_name='Серия и номер диплома или аттестата')
    ielts = models.ImageField(upload_to='ielts/', null=True, verbose_name='Сертификат о знании иностранного языка', help_text='IELTS Если если имеется', blank=True)
    create_at = models.DateTimeField(auto_now_add=True, null=True)
    update_at = models.DateTimeField(auto_now=True, null=True)

    status = models.CharField(max_length=50, choices=STATUS, default='New', blank=True, null=True, verbose_name='Статус')

    phone = models.CharField( max_length=13, verbose_name='Номер телефона')

    def __str__(self):
        return f"{self.last_name}|{self.first_name}|{self.branch_name}==>{self.status}"
