# Generated by Django 3.2.4 on 2021-08-21 06:59

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0013_auto_20210817_2109'),
        ('magistr', '0004_auto_20210819_1950'),
    ]

    operations = [
        migrations.AlterField(
            model_name='magapplication',
            name='nationality',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='home.nationality', verbose_name='Национальность'),
        ),
        migrations.AlterField(
            model_name='magapplication',
            name='phone',
            field=models.CharField(max_length=13, verbose_name='Номер телефона'),
        ),
    ]
