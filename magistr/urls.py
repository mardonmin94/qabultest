from django.urls import path, include
from .views import m_application_view, load_cities

urlpatterns = [
    path('add/', m_application_view, name='add_magistr'),
    path('ajax/load-cities/', load_cities, name='ajax_load_cities'),
]