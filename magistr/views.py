from django.shortcuts import render, redirect
from .models import Magapplication, Branch
from .forms import Magapplicationform
from django.contrib import messages
from home.models import Region, Tuman
from .models import Magapplication
from .forms import Magapplicationform

# Create your views here.

def m_application_view(request):
    if request.method == "POST":
        form = Magapplicationform(request.POST, request.FILES)
        if form.is_valid():
            print(request.POST)
            form.save()
            # data = Magapplication
            # data.branch_name = form.cleaned_data['branch_name']
            # data.last_name = form.cleaned_data['last_name']
            # data.first_name = form.cleaned_data['first_name']
            # data.sure_name = form.cleaned_data['sure_name']
            # data.date_of_birth = form.cleaned_data['date_of_birth']
            # data.gender = form.cleaned_data['gender']
            # data.nationality = form.cleaned_data['nationality']
            # data.photo = form.cleaned_data['photo']
            # data.country = form.cleaned_data['country']
            # data.region = form.cleaned_data['region']
            # data.tuman = form.cleaned_data['tuman']
            # data.adress = form.cleaned_data['adress']
            # data.pasport = form.cleaned_data['pasport']
            # data.pasport_image = form.cleaned_data['pasport_image']
            # data.name_institution = form.cleaned_data['name_institution']
            # data.year = form.cleaned_data['year']
            # data.diplom = form.cleaned_data['diplom']
            # data.diplom_photo = form.cleaned_data['diplom_photo']
            # data.ielts = form.cleaned_data['ielts']
            # data.phone = form.cleaned_data['phone']
            # data.save()
            messages.success(request, 'OK!!!')
        else:
            messages.success(request, 'NONONONONONONO!!!')
        return redirect('add_magistr')
    form = Magapplicationform()
    context = {
        'form': form
    }
    return render(request, 'magistr.html', context)


def load_cities(request):
    region_id = request.GET.get('region_id')
    tumans = Tuman.objects.filter(region_id=region_id).all()
    return render(request, 'city_dropdown_list_options.html', {'tumans': tumans})
    # return JsonResponse(list(cities.values('id', 'name')), safe=False)