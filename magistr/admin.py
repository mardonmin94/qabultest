from django.contrib import admin
from .models import Magapplication, Branch

# Register your models here.

admin.site.register(Magapplication)
admin.site.register(Branch)
