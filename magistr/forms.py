from django.forms import ModelForm, DateInput, Select
from .models import Magapplication, Branch
from home.models import Region, Tuman

class Magapplicationform(ModelForm):
    class Meta:
        model = Magapplication
        fields = ['branch_name', 'last_name', 'first_name', 'sure_name', 'date_of_birth', 'gender', 'nationality',
                  'photo','country', 'region', 'tuman', 'adress', 'pasport', 'pasport_image',
                  'name_institution', 'year', 'diplom', 'diplom_photo', 'ielts', 'phone', ]
        widgets = {
            'date_of_birth': DateInput(attrs={'type': 'date'}),
            # 'region': Select(attrs={'class': 'input'}),
            # 'tuman': Select(attrs={'class': 'input'}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['tuman'].queryset = Tuman.objects.none()

        if 'region' in self.data:
            try:
                region_id = int(self.data.get('region'))
                self.fields['tuman'].queryset = Tuman.objects.filter(region_id=region_id).order_by('name')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields['tuman'].queryset = self.instance.region.tuman_set.order_by('name')
