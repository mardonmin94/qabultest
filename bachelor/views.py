from django.shortcuts import render, redirect, get_object_or_404
from .models import Application, Branch
from .forms import ApplicationForm, SearchForm
from django.contrib import messages
from home.models import Region, Tuman
from django.http import HttpResponse, HttpResponseRedirect
from easy_pdf.views import PDFTemplateView, PDFTemplateResponseMixin
from django.views.generic import DetailView, TemplateView

from django.http import FileResponse
import io
from reportlab.pdfgen import canvas
from reportlab.lib.units import inch
from reportlab.lib.pagesizes import letter

# Create your views here.
def b_application_view(request):
    # form = ApplicationForm()
    if request.method == "POST":
        form = ApplicationForm(request.POST, request.FILES)
        if form.is_valid():
            print(request.POST)
            form.save()
            # data = Application()
            # data.branch_name = form.cleaned_data['branch_name']
            # data.last_name = form.cleaned_data['last_name']
            # data.first_name = form.cleaned_data['first_name']
            # data.sure_name = form.cleaned_data['sure_name']
            # data.date_of_birth = form.cleaned_data['date_of_birth']
            # data.gender = form.cleaned_data['gender']
            # data.nationality = form.cleaned_data['nationality']
            # data.photo = form.cleaned_data['photo']
            # data.country = form.cleaned_data['country']
            # data.region = form.cleaned_data['region']
            # data.tuman = form.cleaned_data['tuman']
            # data.adress = form.cleaned_data['adress']
            # data.pasport = form.cleaned_data['pasport']
            # data.pasport_image = form.cleaned_data['pasport_image']
            # data.type_institution = form.cleaned_data['type_institution']
            # data.name_institution = form.cleaned_data['name_institution']
            # data.year = form.cleaned_data['year']
            # data.diplom = form.cleaned_data['diplom']
            # data.diplom_photo = form.cleaned_data['diplom_photo']
            # data.ielts = form.cleaned_data['ielts']
            # data.phone = form.cleaned_data['phone']
            # data.save()
            messages.success(request, 'OK!!!')
        else:
            messages.success(request, 'NONONONON')
        return redirect('add_bachelor')

    form = ApplicationForm()
    # branch = Branch.objects.all()
    context = {
        'form': form,
    }
    return render(request, 'bachelor.html', context)

######

def list_b_view(request):
    search_query = request.GET.get('search','')
    if search_query:
       app = Application.objects.filter(pasport__icontains=search_query)
    else:
        app = Application.objects.all()
    return render(request, 'list_b.html', {'app':app})


def abitur_page(request, id):
    app = get_object_or_404(Application, pk=id)
    # my = Application.objects.get(pk=id)
    return render(request, 'abitur_page.html', {'app': app})


class PdfDetail(PDFTemplateResponseMixin, DetailView):
    template_name = 'pdf_detail.html'
    context_object_name = 'app'
    model = Application
    # encoding = u"utf-8"




def load_cities(request):
    region_id = request.GET.get('region_id')
    tumans = Tuman.objects.filter(region_id=region_id).all()
    return render(request, 'city_dropdown_list_options.html', {'tumans': tumans})
    # return JsonResponse(list(cities.values('id', 'name')), safe=False)


# def search(request):
#     if request.method == 'POST':
#         form = SearchForm(request.POST)
#         if form.is_valid():
#             query = form.cleaned_data['query']
#             catid = form.cleaned_data['catid']
#             if catid == 0:
#                 apps = Application.objects.filter(pasport__icontains=query)
#             else:
#                 apps = Application.objects.filter(pasport__icontains=query, branch_name_id=catid)
#             barnch = Branch.objects.all()
#             context = {
#                     'apps': apps,
#                     'query': query,
#                     'branch': barnch,
#             }
#             return render(request, 'search.html', context)
#     return render(request, 'list_b.html')
