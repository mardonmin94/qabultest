from django.contrib import admin
from .models import Application, Branch

# Register your models here.

class ApplicationAdmin(admin.ModelAdmin):
    list_display = ['last_name', 'first_name', 'branch_name', 'phone', 'create_at', 'status']
    list_filter = ['branch_name', 'status']

admin.site.register(Application, ApplicationAdmin)
admin.site.register(Branch)
