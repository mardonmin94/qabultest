# Generated by Django 3.2.4 on 2021-08-14 07:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0010_auto_20210813_2348'),
        ('bachelor', '0002_auto_20210813_2348'),
    ]

    operations = [
        migrations.AlterField(
            model_name='application',
            name='country',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='home.country', verbose_name='Гражданство'),
        ),
        migrations.AlterField(
            model_name='application',
            name='district',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='home.district', verbose_name='Район'),
        ),
        migrations.AlterField(
            model_name='application',
            name='region',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='home.region', verbose_name='Область'),
        ),
        migrations.AlterField(
            model_name='application',
            name='year',
            field=models.CharField(max_length=4, null=True, verbose_name='Год выпуска'),
        ),
    ]
