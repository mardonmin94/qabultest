# Generated by Django 3.2.4 on 2021-08-18 12:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bachelor', '0009_application_pasport_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='application',
            name='diplom_photo',
            field=models.FileField(null=True, upload_to='diplom/', verbose_name='Электронный образ диплома или аттестата'),
        ),
        migrations.AddField(
            model_name='application',
            name='ielts',
            field=models.FileField(blank=True, help_text='IELTS Если если имеется', null=True, upload_to='ielts/', verbose_name='Сертификат о знании иностранного языка'),
        ),
        migrations.AlterField(
            model_name='application',
            name='photo',
            field=models.ImageField(null=True, upload_to='photo/', verbose_name='Фотография 3х4 (jpg, jpeg, png)'),
        ),
    ]
