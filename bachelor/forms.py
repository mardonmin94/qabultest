from .models import Application, Region, Tuman, Country
from django import forms
from django.forms import ModelForm, TextInput, Form, Select, FileInput, DateInput


class ApplicationForm(ModelForm):
    class Meta:
        model = Application
        fields = ['branch_name', 'last_name', 'first_name', 'sure_name', 'date_of_birth', 'gender', 'nationality', 'photo',
                  'country', 'region', 'tuman', 'adress', 'pasport', 'pasport_image',
                  'type_institution', 'name_institution', 'year', 'diplom', 'diplom_photo', 'ielts', 'phone',]
        widgets = {
            # 'branch_name' : Select(attrs={'class':'input'}),
            # 'last_name' : TextInput(attrs={'class':'input2',}),
            # 'sure_name': TextInput(attrs={'class':'input2'}),
            # 'first_name': TextInput(attrs={'class':'input2'}),
            'date_of_birth': DateInput(attrs={'type':'date'}),
            # 'gender': Select(attrs={'class': 'input'}),
            # 'nationality': Select(attrs={'class':'input'}),
            # 'photo': FileInput(attrs={'class':'file-upload'}),
            # 'country': Select(attrs={'class':'input'}),
            # 'region': Select(attrs={'class':'input'}),
            # 'tuman': Select(attrs={'class':'input'}),
            # 'adress': TextInput(attrs={'class':'input'}),
            # 'pasport': TextInput(attrs={'class':'input'}),
            # 'pasport_image': FileInput(attrs={'class':'file-upload'}),
            # 'type_institution': Select(attrs={'class':'input'}),
            # 'name_institution': TextInput(attrs={'class':'input'}),
            # 'year': TextInput(attrs={'class':'input'}),
            # 'diplom': TextInput(attrs={'class':'input'}),
            # 'diplom_photo': FileInput(attrs={'class':'file-upload'}),
            # 'ielts': FileInput(attrs={'class':'file-upload'}),
            # 'phone': TextInput(attrs={'class':'input'}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['tuman'].queryset = Tuman.objects.none()

        if 'region' in self.data:
            try:
                region_id = int(self.data.get('region'))
                self.fields['tuman'].queryset = Tuman.objects.filter(region_id=region_id).order_by('name')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields['tuman'].queryset = self.instance.region.tuman_set.order_by('name')


class SearchForm(forms.Form):
    query = forms.CharField(max_length=100)
    catid = forms.IntegerField()
