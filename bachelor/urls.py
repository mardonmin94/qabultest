from django.urls import path, include
from .views import b_application_view, load_cities, abitur_page, PdfDetail, list_b_view
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('add', b_application_view, name='add_bachelor'),
    # path(r'^p/(?P<id>\d+)$', abitur_page, name='abitur_detail'),
    path('', list_b_view, name='list_b'),
    # path('list/<int:id>', abitur_page, name='abitur_view'),
    # path('search/', search, name='search'),
    path('<int:pk>', PdfDetail.as_view(), name='ruxsatnoma'),
    # path('page_pdf/', page_pdf, name='page_pdf'),
    path('ajax/load-cities/', load_cities, name='ajax_load_cities'),
]

if settings.DEBUG:
    # urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
